<?php
  session_start();
  // recuperation du pseudo
  if (isset($_REQUEST["pseudo"])) {
      $pseudo = $_SESSION["pseudo"] = $_REQUEST["pseudo"];
  } elseif (isset($_SESSION["pseudo"])) {
      $pseudo = $_SESSION["pseudo"] ;
  } else {
      $pseudo = '';
  }
  // recuperation de l'id du chat
  if (isset($_REQUEST["chat"])) {
      $chatId = $_SESSION["chat"] = $_REQUEST["chat"];       
  } elseif (isset($_SESSION["chat"])) {
      $chatId = $_SESSION["chat"] ;  
  }     
  if (empty ($chatId)) $chatId = 1;
  // en cas de login on met a jour la session
  if (isset($_REQUEST["login"])) {
      session_write_close();
  } else {
      session_abort();
  }

  // connexion a la base de donnees 
  try {
    $base = new PDO('mysql:host=mysql;dbname=chat;charset=UTF8;', 'root', file_get_contents('/run/secrets/mysql/password'));
  } catch(exception $e) {
    die('Erreur '.$e->getMessage());
  }
  // recupération du nom du chat
  $base->exec("SET CHARACTER SET utf8");
  $res = $base->query('SELECT name FROM chat where id = '.$chatId);
  if ($data = $res->fetch()) {
    $chatName = $data['name'];
  }
  // ajout nouveau message dans la base
  if (isset($_REQUEST["message"]) && isset($_REQUEST["send"])  
      && ! empty($chatName) && ! empty($chatId)) {
    try {
      $base->exec("SET CHARACTER SET utf8");
      $stmt = $base->prepare('INSERT INTO message (pseudo, content, chat) VALUES (?, ?, ?)');
      $base->beginTransaction();
      $stmt->execute([$pseudo, $_REQUEST["message"], $chatId]);
      $base->commit();
    } catch(exception $e) {
      die('Erreur '.$e->getMessage());
    }
 }
?>
<html>
<head>
    <title>Chat: <?= $chatName ?></Title>
    <meta charset="UTF-8">
</head>
<body>
    <h1>Chat: <span><?= $chatName ?></span></h1>
    <p>Vous êtes connecté sous le pseudo : <span><?= $pseudo ?></span></p>
    <p>Retour à la <a href="index.html">page d'accueil</a></p>
    <hr>
    <form method="POST" >
        <p>Poster un message : </p>
        <p><span>Message : </span>
            <textarea style="vertical-align: top;" rows="6" cols="50" name="message"></textarea>
            <input style="vertical-align: bottom;" type="submit" name="send" value="Envoyer" />
        </p>
     </form>
    <hr>
    <?php
      // recuperation des messages du chat
      $res = $base->query('SELECT * FROM message where chat = '.$chatId." ORDER BY date_mesg ASC") ;
      while ($data = $res->fetch()) {
        $message = $data['content'];
        $date_mesg = date_format(date_create($data['date_mesg'], new DateTimeZone('UTC')), 'H:i:s');
        $sender = $data['pseudo'];
    ?> 
    <div><span><?= $date_mesg ?></span> - <span><?= $sender ?> : </span><span ><?= $message ?></span</div>
    <?php
      }
      // fermeture de la connexion 
      $res = null;
      $base = null;
    ?> 
  </body>
</html>